

package seng271a2;

import java.awt.*;
import java.lang.reflect.Method;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ButtonPanelTop extends JPanel implements CalculatorColors, ActionListener
{
    JButton backSpace, clearEntry, clear;
    CalculatorModel model;
    Class  thisClass;   // for use in reflection below...
    
    ButtonPanelTop(CalculatorModel model)
    {
        thisClass = this.getClass();    // for use in reflection ...
        this.model = model;
        
        backSpace = new JButton("BackSpace");
        backSpace.setForeground(specialButtonColorText);
        backSpace.setBackground(buttonBackgroundColor);
        backSpace.setMargin(new Insets(6, 4, 6, 4));
        backSpace.addActionListener(this);;
        this.add(backSpace);
        
        clearEntry = new JButton("CE");
        clearEntry.setForeground(specialButtonColorText);
        clearEntry.setBackground(buttonBackgroundColor);
        clearEntry.setMargin(new Insets(6, 29, 6, 29));
        clearEntry.addActionListener(this);
        this.add(clearEntry);
        
        clear = new JButton("C");
        clear.setForeground(specialButtonColorText);
        clear.setBackground(buttonBackgroundColor);
        clear.setMargin(new Insets(6, 29, 6, 29));
        clear.addActionListener(this);  // add ButtonPanelTop as an action listener to clear button
        this.add(clear);
        
        this.setBackground(backgroundColor);
    }
    
    public void actionPerformed(ActionEvent e)
    {
        if ( e.getSource() == backSpace )
        {
            model.backSpace();
        }
        else if ( e.getSource() == clearEntry )
        {
            model.clearEntry();
        }
        else if ( e.getSource() == clear )
        {
            model.clearValue();
        }
    }
}   // end of class ButtonPanelTop extends JPanel implements CalculatorColors, ActionListener
