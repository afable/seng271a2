package seng271a2;


import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JRadioButtonMenuItem;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Point;

/**
 *  Description: Frame is the top level layer of the Swing application. Everything
 *  exists within a JFrame.
 *
 */
class BasicCalculatorFrame extends JFrame implements CalculatorColors
{
    // the one model of data and logic for VicCalc
    //CalculatorModel model;    // model is passed in to constructor as a parameter

    // the three output panels views that depend on the one model
    OutputPanelDefault opDefault;   // passed in to constructor as paramter
    OutputPanelYellow opYellow;    // passed in to constructor as paramter
    OutputPanelFunky opFunky; // passed in to constructor as paramter

    // the two view/controllers that take in user events
    ButtonPanelTop top;     // top is passed in to constructor as a parameter
    ButtonPanelMain main;       // main is passed in to constructor as a parameter
    
    JMenuBar jmb;
    JMenu editMenu, viewMenu, helpMenu, notNecessaryMenu;

    BasicCalculatorFrame()
    {

    }
    
    BasicCalculatorFrame(String header, CalculatorModel model,
            OutputPanelDefault opDefault, OutputPanelYellow opYellow, OutputPanelFunky opFunky,
            ButtonPanelTop top, ButtonPanelMain main, Point location)
    {
        // call JFrame with string title of frame
        super(header);

        // Create a container to hold all of our control and view components
        Container cp = getContentPane();

        // flow needs to be set before adding components (how will the frame's components be layed out?)
        FlowLayout layout = new FlowLayout();
        //layout.setAlignment(FlowLayout.CENTER);   // default
        //layout.setVgap(5);    // default
        //layout.setHgap(5);    // default
        cp.setLayout(layout);   // set the layout
        cp.setBackground(backgroundColor);  // set the backgroundcolor


        /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         *
         * Create and set Views.
         *
         *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         */
        // set the three output panels to the output panels passed in to constructor
        this.opDefault = opDefault;
        this.opYellow = opYellow;
        this.opFunky = opFunky;
        cp.add(opDefault);
        cp.add(opYellow);
        cp.add(opFunky);
        
        // Ensure any changes in the Model (the "subject") will be "Observed"
        // by the View (i.e., the "update()" method in the View will be triggered
        // when the model indicates a changed state)
        model.addObserver(opDefault);
        model.addObserver(opYellow);
        model.addObserver(opFunky);


        /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         *
         * Create and set Controllers.
         *
         *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         */
        // We set this class' controller pointers to those passed in to the constructor.
        // Our two complementary controllers must have references to the model.
        this.top = top;
        this.main = main;
        cp.add(top);    // add these components to the JFrame
        cp.add(main);       // add these components to the JFrame
        

        /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         *
         * Create Menu items. People just seem to be calmer if they see menu
         * items, so we will display them but they will do nothing.
         *
         *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         */
        jmb = new JMenuBar();
        jmb.setBackground(backgroundColor);
        
        editMenu = new JMenu("Edit", true);
        editMenu.setBackground(backgroundColor);
        viewMenu = new JMenu("View", true);
        viewMenu.setBackground(backgroundColor);
        helpMenu = new JMenu("Help", true);
        helpMenu.setBackground(backgroundColor);
        notNecessaryMenu = new JMenu("Are Not Necessary", true);
        notNecessaryMenu.setBackground(backgroundColor);

        JRadioButtonMenuItem agree = new JRadioButtonMenuItem("I agree");
        JRadioButtonMenuItem disagree = new JRadioButtonMenuItem("You're wrong! They help me sleep at night");
        notNecessaryMenu.add(agree);
        notNecessaryMenu.add(disagree);
        
        jmb.add(editMenu);
        jmb.add(viewMenu);
        jmb.add(helpMenu);
        jmb.add(notNecessaryMenu);
        
        this.setJMenuBar(jmb);
        
        /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         *
         * Set JFrame window properties.
         *
         *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         */
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);   // exit program when close button pressed
        this.setSize(400, 420); // set this JFrame's size big enough to hold all of its components
        this.setVisible(true);  // show this JFrame
        this.setResizable(true);   // you can resize this JFrame if you wish
        this.setLocation(location);
    }
}   // end of class BasicCalculatorFrame extends JFrame implements CalculatorColors

