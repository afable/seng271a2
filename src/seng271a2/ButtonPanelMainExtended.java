package seng271a2;

import java.awt.*;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

class ButtonPanelMainExtended extends JPanel implements CalculatorColors, ActionListener
{
    JButton memoryClear, memoryRecall, memoryStore, memoryPlus, memoryExchange;
    JButton plus, minus, times, divide, equals;
    JButton bitwiseAnd, bitwiseOr, reciprocal;
    JButton signChange, decimalPoint;
    JButton digits[];
    JButton blanks[]; // placeholders to help organize JButton component layout
    
    CalculatorModel model;

    private final int MAX_BLANKS = 6;
    
    ButtonPanelMainExtended(CalculatorModel model)
    {
        this.model = model;
        createButtons();
        arrangeButtons();
    }

    /*
     * Description: Event triggered for any JButtons that have added this
     * ButtonPanelMain as an action listener.
     */
    public void actionPerformed(ActionEvent e)
    {


        /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         *
         * Test memtrueory buttons: test for MC, MR, MS, M+ buttons
         *
         *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         */
        if ( e.getSource() == memoryClear )
        {
            model.memoryClear();
        }
        else if ( e.getSource() == memoryRecall )
        {
            model.memoryRecall();
        }
        else if ( e.getSource() == memoryStore )
        {
            model.memoryStore();
        }
        else if ( e.getSource() == memoryPlus )
        {
            model.memoryPlus();
        }
        else if ( e.getSource() == memoryExchange )
        {
            model.memoryExchange();
        }

        

        /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         *
         * Test operator buttons: test for /, *, -, +sqrt, %, reciprocal, = buttons
         *
         *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         */
        if ( e.getSource() == divide )
        {
            model.divide();
        }
        else if ( e.getSource() == times )
        {
            model.times();
        }
        else if ( e.getSource() == minus )
        {
            model.minus();
        }
        else if ( e.getSource() == plus )
        {
            model.plus();
        }
        else if ( e.getSource() == bitwiseAnd )
        {
            model.bitwiseAnd();
        }
        else if ( e.getSource() == bitwiseOr )
        {
            model.bitwiseOr();
        }
        else if ( e.getSource() == reciprocal )
        {
            model.reciprocal();
        }
        else if ( e.getSource() == equals )
        {
            model.equals();
        }
        else if ( e.getSource() == signChange )
        {
            model.signChange();
        }
        else if ( e.getSource() == decimalPoint )
        {
            model.decimalPoint();
        }

        
        /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         *
         * Test number pad buttons: test for 0 through 9 digit buttons
         *
         *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         */
        for ( int i = 0; i < 10; ++i )
        {
            if (e.getSource() == digits[i]) // since digits[i] is an array of JButtons
            {
                model.appendDigit(Integer.toString(i));
                return;
            }
        }
        
    }
    
    
    private void createButtons()
    {
        Insets buttonMargin = new Insets(5, 1, 5, 1);   // margins between buttons top, left, bottom, right


        /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         *
         * Create memory buttons
         *
         *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         */
        memoryClear = new JButton("MC");
        memoryClear.setBackground(buttonBackgroundColor);
        memoryClear.setForeground(specialButtonColorText);
        memoryClear.setMargin(buttonMargin);
        memoryClear.addActionListener(this);
       
        memoryRecall = new JButton("MR");
        memoryRecall.setBackground(buttonBackgroundColor);
        memoryRecall.setForeground(specialButtonColorText);
        memoryRecall.setMargin(buttonMargin);
        memoryRecall.addActionListener(this);
        
        memoryStore = new JButton("MS");
        memoryStore.setBackground(buttonBackgroundColor);
        memoryStore.setForeground(specialButtonColorText);
        memoryStore.setMargin(buttonMargin);
        memoryStore.addActionListener(this);
        
        memoryPlus = new JButton("M+");
        memoryPlus.setBackground(buttonBackgroundColor);
        memoryPlus.setForeground(specialButtonColorText);
        memoryPlus.setMargin(buttonMargin);
        memoryPlus.addActionListener(this);

        memoryExchange = new JButton("MX");
        memoryExchange.setBackground(buttonBackgroundColor);
        memoryExchange.setForeground(specialButtonColorText);
        memoryExchange.setMargin(buttonMargin);
        memoryExchange.addActionListener(this);


        /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         *
         * Create operator buttons
         *
         *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         */
        plus = new JButton("+");
        plus.setBackground(buttonBackgroundColor);
        plus.setForeground(specialButtonColorText);
        plus.setMargin(buttonMargin);
        plus.addActionListener(this);
        
        minus = new JButton("-");
        minus.setBackground(buttonBackgroundColor);
        minus.setForeground(specialButtonColorText);
        minus.setMargin(buttonMargin);
        minus.addActionListener(this);
        
        times = new JButton("*");
        times.setBackground(buttonBackgroundColor);
        times.setForeground(specialButtonColorText);
        times.setMargin(buttonMargin);
        times.addActionListener(this);
        
        divide = new JButton("/");
        divide.setBackground(buttonBackgroundColor);
        divide.setForeground(specialButtonColorText);
        divide.setMargin(buttonMargin);
        divide.addActionListener(this);
        
        bitwiseAnd = new JButton("&");
        bitwiseAnd.setBackground(buttonBackgroundColor);
        bitwiseAnd.setForeground(normalButtonColorText);
        bitwiseAnd.setMargin(buttonMargin);
        bitwiseAnd.addActionListener(this);

        // bitwiseOr = modular operator
        bitwiseOr = new JButton("|");
        bitwiseOr.setBackground(buttonBackgroundColor);
        bitwiseOr.setForeground(normalButtonColorText);
        bitwiseOr.setMargin(buttonMargin);
        bitwiseOr.addActionListener(this);
        
        reciprocal = new JButton("");
        reciprocal.setBackground(backgroundColor);    // set to background color to hide button
        reciprocal.setForeground(normalButtonColorText);
        reciprocal.setMargin(buttonMargin);
        reciprocal.setEnabled(false);   // disable this button
        reciprocal.setBorder(BorderFactory.createEmptyBorder());  // remove border so button doesn't show
        //reciprocal.addActionListener(this);   // button is not enabled, so do not add an actionlistener to it
        
        equals = new JButton("=");
        equals.setBackground(buttonBackgroundColor);
        equals.setForeground(specialButtonColorText);
        equals.setMargin(buttonMargin);
        equals.addActionListener(this);

        decimalPoint = new JButton(".");
        decimalPoint.setBackground(buttonBackgroundColor);
        decimalPoint.setForeground(specialButtonColorText);
        //decimalPoint.setBorder(BorderFactory.createEmptyBorder());  // remove border so button doesn't show
        decimalPoint.setMargin(buttonMargin);
        decimalPoint.addActionListener(this);
        
        signChange = new JButton("+/-");
        signChange.setBackground(buttonBackgroundColor);
        signChange.setForeground(normalButtonColorText);
        signChange.setMargin(buttonMargin);
        signChange.addActionListener(this);


        /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         *
         * Create number pad buttons
         *
         *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         */
        digits = new JButton[10];
        for ( int i = 0; i < 10; ++i )
        {
            String buttonText = Integer.toString(i);
            digits[i] = new JButton(buttonText);
            digits[i].setBackground(buttonBackgroundColor);
            digits[i].setForeground(normalButtonColorText);
            digits[i].setMargin(buttonMargin);
            digits[i].addActionListener(this);  // create an actionListener for each digit
        }


        /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         *
         * Create blanks that are used as placeholders to help organize layout
         *
         *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         */
        blanks = new JButton[MAX_BLANKS];
        for ( int i = 0; i < MAX_BLANKS; ++i )
        {
            blanks[i] = new JButton("");
            blanks[i].setBackground(backgroundColor);    // set to background color to hide button
            blanks[i].setForeground(normalButtonColorText);
            blanks[i].setMargin(buttonMargin);
            blanks[i].setEnabled(false);   // disable this button
            blanks[i].setBorder(BorderFactory.createEmptyBorder());  // remove border so button doesn't show
            //blanks[i].addActionListener(this);   // button is not enabled, so do not add an actionlistener to it
        }
    }
    
    private void arrangeButtons()
    {
        this.setBackground(backgroundColor);
        this.setLayout(new GridLayout(5, 6, 5, 5)); // 5 rows, 6 cols, 5 hgap, 5 vgap
        
        // Lay out the top row -- should contains six items
        this.add(memoryClear);
        this.add(digits[7]);
        this.add(digits[8]);
        this.add(digits[9]);
        this.add(divide);
        this.add(bitwiseAnd);
        
        // Now lay out the second row -- should contain six items
        this.add(memoryRecall);
        this.add(digits[4]);
        this.add(digits[5]);
        this.add(digits[6]);
        this.add(times);
        this.add(bitwiseOr);
        
        // Now lay out the third row -- should contain six items
        this.add(memoryStore);
        this.add(digits[1]);
        this.add(digits[2]);
        this.add(digits[3]);
        this.add(minus);
        this.add(reciprocal);
        
        // Now lay out the fourth row...
        this.add(memoryPlus);
        this.add(digits[0]);
        this.add(signChange);
        this.add(decimalPoint);
        this.add(plus);
        this.add(equals);

        // Now lay out the final (fifth) row...
        this.add(memoryExchange);
        this.add(blanks[1]);
        this.add(blanks[2]);
        this.add(blanks[3]);
        this.add(blanks[4]);
        this.add(blanks[5]);
    }





}   // end of class ButtonPanelMain extends JPanel implements CalculatorColors, ActionListener







