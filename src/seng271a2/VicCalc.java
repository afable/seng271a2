/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package seng271a2;

import javax.swing.*;
import java.awt.Point;

/**
 *
 * @author eafable
 */
public class VicCalc
{
    // create a new instance of VicCalc
    public VicCalc()
    {
        // not used
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
            UIManager.setLookAndFeel(
                    UIManager.getCrossPlatformLookAndFeelClassName()
//                      UIManager.getSystemLookAndFeelClassName()
                    );
        } catch (UnsupportedLookAndFeelException e) {
        } catch (ClassNotFoundException cnfe) {
        } catch (InstantiationException ie) {
        } catch (IllegalAccessException iae) {
        }

        // create the one model that everything is based on
        CalculatorModel model = new CalculatorModel();

        // create the three output panels for both default and extended calculators that act as views
        OutputPanelDefault opDefault = new OutputPanelDefault();
        OutputPanelYellow opYellow = new OutputPanelYellow();
        OutputPanelFunky opFunky = new OutputPanelFunky();
        OutputPanelDefault opDefaultExtended = new OutputPanelDefault();
        OutputPanelYellow opYellowExtended = new OutputPanelYellow();
        OutputPanelFunky opFunkyExtended = new OutputPanelFunky();

        // create the view/controllers that take in use data
        ButtonPanelTop top = new ButtonPanelTop(model);
        ButtonPanelMain mainDefault = new ButtonPanelMain(model);
        ButtonPanelTop topExtended = new ButtonPanelTop(model);
        ButtonPanelMainExtended mainExtended = new ButtonPanelMainExtended(model);

        // create a Point for the window location of the default and extended calculators
        Point pDefault = new Point(0, 0);
        Point pExtended = new Point(420, 0);    // set extended calculator window right of default

        
        
        // create a JFrame for the first set of controllers (Main & Top)
        BasicCalculatorFrame bcf = new BasicCalculatorFrame("Default VicCalc | eafable (v00692209)", model, opDefault, opYellow, opFunky, top, mainDefault, pDefault);

        // create a JFrame for the textended controller (MainExtended & Top)
        BasicCalculatorFrameExtended ecf = new BasicCalculatorFrameExtended("Extended VicCalc | eafable (v00692209)", model, opDefaultExtended, opYellowExtended, opFunkyExtended, topExtended, mainExtended, pExtended);
    }

}   // end of class VicCalc
