/*
 * CalculatorColors.java
 *
 */

package seng271a2;

import java.awt.Color;


/**
 *  Description: A place holder for all of the colors used in the VicCalc program.
 *  Not really sure why we would use an interface where a class (struct) would suffice.
 *
 */
public interface CalculatorColors
{
    final Color backgroundColor = new Color(241, 237, 222);
    final Color buttonBackgroundColor = new Color(242, 244, 241);
    final Color specialButtonColorText = Color.red; // backspace, ce, c, mc, mr, ms, m+, operators, =
    final Color normalButtonColorText = Color.blue; // numbers
    
}   // end of interface CalculatorColors
