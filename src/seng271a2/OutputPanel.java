package seng271a2;

import java.awt.*;
import java.util.Observer;
import java.util.Observable;
import javax.swing.*;

public class OutputPanel extends JPanel implements CalculatorColors, Observer
{
    JTextField output;
    
    final public static int TEXT_FIELD_WIDTH = 28;
    final public static String OVERFLOW_CHAR = "#";
    
    OutputPanel()
    {
        output = new JTextField("0", TEXT_FIELD_WIDTH);
        output.setFont(new Font("Courier", Font.PLAIN, 16));
        output.setHorizontalAlignment(4);
        
        // Place in the panel
        this.add(output);
        this.setBackground(backgroundColor);
    }
    
    public void update(Observable model, Object obj)
    {
        String value = ((CalculatorModel)model).getValue();
        output.setText(value);
    }
}   // end of class OutputPanel extends JPanel implements CalculatorColors, Observer
