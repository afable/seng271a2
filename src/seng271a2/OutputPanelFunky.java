package seng271a2;

import java.awt.*;
import java.util.Observer;
import java.util.Observable;
import javax.swing.*;

public class OutputPanelFunky extends JPanel implements CalculatorColors, Observer
{
    JTextField output;
    
    final public static int TEXT_FIELD_WIDTH = 12;
    final public static int TEXT_FIELD_SIZE = 28;
    final public static String OVERFLOW_CHAR = "#";
    
    OutputPanelFunky()
    {
        output = new JTextField("0", TEXT_FIELD_WIDTH);
        output.setFont(new Font("utkal", Font.ITALIC | Font.BOLD, TEXT_FIELD_SIZE));
        output.setHorizontalAlignment(JTextField.LEFT);
        output.setBackground(Color.MAGENTA);
        output.setForeground(Color.CYAN);   // the actual string text color
        
        // Place in the panel
        this.add(output);
        this.setBackground(backgroundColor);
    }

    /**
     * Description: update method notified when observers change in model-view paradigm
     *
     * An observable object can have one or more observers. An observer may be
     * any object that implements interface Observer. After an observable
     * instance changes, an application calling the Observable's notifyObservers
     * method causes all of its observers to be notified of the change by a
     * call to their update method.
     *
     * Basically, we generally don't have to change this since the output panel
     * is always observing the calculator model and will always display the
     * proper output when the calculator model notifies observers.
     */
    public void update(Observable model, Object obj)
    {
        String value = ((CalculatorModel)model).getValue();
        output.setText(value);
    }
}   // end of class OutputPanel extends JPanel implements CalculatorColors, Observer
