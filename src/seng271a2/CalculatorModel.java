/*
 * CalculatorModel.java
 *
 * Created on June 11, 2007, 8:06 PM
 * Modified on June 10, 2012, 9:28 PM
 */

package seng271a2;

import java.util.Observable;
import java.util.Observer;
import java.math.BigInteger;
import java.math.*;


/**
 *  Description: The model should contain all the data and logic. For this
 *  assignment, 2 controllers will have a reference to this one model and 3 views
 *  will be updated based on this one model.
 * 
 */
public class CalculatorModel extends Observable
{
    private enum Operator { DIVIDE, TIMES, MINUS, PLUS, AND, OR, EQUALS, MEMORY_RECALL, MEMORY_STORE, MEMORY_EXCHANGE1, MEMORY_EXCHANGE2 }

    private String ERROR_DIVISION_ZERO = new String("Cannot Divide By Zero");
    private StringBuffer displayString;  // better than using String since can be modified real-time & thread-safe
    private BigDecimal result;  // result for current calculator value
    private BigDecimal[] memory;    // store memory values from calculator
    private int currentMemoryIndex;     // the current memory index
    private int memoryExchange1;    // the first memory value to exchange
    private int memoryExchange2;    // the second memory value to exchange
    private BigDecimal swapTemp;    // temp to hold value while swapping memory
    private Operator currentOperator;

    // class constants
    final public static int SCALE_TWO_DECIMAL = 2;  // the amount to scale big decimals (trailing digits after decimal)
    final public static int MAX_MEMORY = 10;
    final public static String INITIAL_VALUE = "0"; // initial and reset value of output


    /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
     *
     * Constructor
     *
     *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
     */
    public CalculatorModel()
    {
        displayString = new StringBuffer(INITIAL_VALUE);
        result = new BigDecimal(0.0f);
        memory = new BigDecimal[MAX_MEMORY];
        for ( int i = 0; i < MAX_MEMORY; ++i )
        {
            memory[i] = new BigDecimal(0.0f);
        }
        currentMemoryIndex = 0;
        currentOperator = Operator.EQUALS;  // equals allows other operators the go ahead
    }

    /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
     *
     * Getters and Setters
     *
     *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
     */
    public String getValue()
    {
        return displayString.toString();
    }


    /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
     *
     * Top Controller Button Messages: messages sent upon an event
     * from the action listeners of ButtonPanelTop
     *
     *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
     */
    // delete the last digit of the display string
    public void backSpace()
    {
        if ( displayString.length() == 1 )
        {
            displayString = new StringBuffer(INITIAL_VALUE);
        }
        else
        {
            displayString.delete(displayString.length()-2, displayString.length()-1);
        }
        setChanged();
        notifyObservers();
    }

    // reset the display but leave result alone
    public void clearEntry()
    {
        displayString = new StringBuffer(INITIAL_VALUE);
        setChanged();
        notifyObservers();
    }
    
    // reset both display and result
    public void clearValue()
    {
        displayString = new StringBuffer(INITIAL_VALUE);
        result = new BigDecimal(new String("0"));
        currentOperator = Operator.EQUALS;
        setChanged(); // Marks this Observable object as having been changed; the hasChanged method will now return true.
        notifyObservers();  // If this object has changed, as indicated by the hasChanged method, then notify all of its observers and then call the clearChanged method to indicate that this object has no longer changed.
    }


    /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
     *
     * Main Controller Button Messages: messages sent upon an event
     * from the action listeners of ButtonPanelMain
     *
     *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
     */
    // append a digit to the display string
    public void appendDigit(String digit)
    {
        // handle memory recall and storage first
        if ( currentOperator == Operator.MEMORY_RECALL )
        {
            // recall memory at digit index and display to output
            currentMemoryIndex = (new BigInteger(digit)).intValue();
            displayString = new StringBuffer(memory[currentMemoryIndex].toString());
            
            // reset current operator
            currentOperator = Operator.EQUALS;
        }
        else if ( currentOperator == Operator.MEMORY_STORE )
        {
            // save display to memory at digit index
            currentMemoryIndex = (new BigInteger(digit)).intValue();
            memory[currentMemoryIndex] = new BigDecimal(displayString.toString());

            // reset current operator
            currentOperator = Operator.EQUALS;
        }
        else if ( currentOperator == Operator.MEMORY_EXCHANGE1 )
        {
            memoryExchange1 = (new BigInteger(digit)).intValue();

            // set current operator to take in the next memory index
            currentOperator = Operator.MEMORY_EXCHANGE2;
        }
        else if ( currentOperator == Operator.MEMORY_EXCHANGE2 )
        {
            memoryExchange2 = (new BigInteger(digit)).intValue();

            // swap the memory values
            swapTemp = memory[memoryExchange1];
            memory[memoryExchange1] = memory[memoryExchange2];
            memory[memoryExchange2] = swapTemp;

            // reset current operator
            currentOperator = Operator.EQUALS;
        }
        // handle regular digit input
        else
        {
            if (displayString.toString().equals(INITIAL_VALUE))
            {
                displayString = new StringBuffer("");
            }
            displayString.append(digit);
        }
        // notify observers of changes
        setChanged();
        notifyObservers();
    }

    // clear the digits stored in all memory blocks
    public void memoryClear()
    {
        for ( int i = 0; i < MAX_MEMORY; ++i )
            memory[i] = new BigDecimal(0.0f);
    }

    // recall the digit stored in the current memory block
    public void memoryRecall()
    {
        currentOperator = Operator.MEMORY_RECALL;
    }

    // store the display string digit to current memory block
    public void memoryStore()
    {
        currentOperator = Operator.MEMORY_STORE;
    }

    // add 1 more memory block
    public void memoryPlus()
    {
        //++MAX_MEMORY;     // already at max memory
    }

    // exchange values between two memory addresses
    public void memoryExchange()
    {
        currentOperator = Operator.MEMORY_EXCHANGE1;
    }

    // set current operator to 'add' (next operator button will operate display string to result)
    public void divide()
    {
        if ( currentOperator == Operator.EQUALS )
        {
            result = new BigDecimal(displayString.toString());
            displayString = new StringBuffer("0");
            setChanged();
            notifyObservers();
            currentOperator = Operator.DIVIDE;
        }
    }

    // set current operator to 'times' (next operator button will operate display string to result)
    public void times()
    {
        if ( currentOperator == Operator.EQUALS )
        {
            result = new BigDecimal(displayString.toString());
            displayString = new StringBuffer("0");
            setChanged();
            notifyObservers();
            currentOperator = Operator.TIMES;
        }
    }

    // set current operator to 'minus' (next operator button will operate display string to result)
    public void minus()
    {
        if ( currentOperator == Operator.EQUALS )
        {
            result = new BigDecimal(displayString.toString());
            displayString = new StringBuffer("0");
            setChanged();
            notifyObservers();
            currentOperator = Operator.MINUS;
        }
    }

    // set current operator to 'plus' (next operator button will operate display string to result)
    public void plus()
    {
        if ( currentOperator == Operator.EQUALS )
        {
            result = new BigDecimal(displayString.toString());
            displayString = new StringBuffer("0");
            setChanged();
            notifyObservers();
            currentOperator = Operator.PLUS;
        }
    }

    // set current operator to 'percent' (next operator button will operate display string to result)
    public void percent()
    {
        result = new BigDecimal(displayString.toString());
        result = result.divide(new BigDecimal(100.0f), SCALE_TWO_DECIMAL, BigDecimal.ROUND_HALF_UP);
        displayString = new StringBuffer(result.toString());
        setChanged();
        notifyObservers();
    }

    // unary operator square root, so operate on display string immediately
    public void squareRoot()
    {
        result = new BigDecimal(displayString.toString());
        result = new BigDecimal(Math.sqrt(result.doubleValue()));
        result = result.setScale(SCALE_TWO_DECIMAL, BigDecimal.ROUND_HALF_UP);
        displayString = new StringBuffer(result.toString());
        setChanged();
        notifyObservers();
    }

    // reciprocal is a unary operator so operate on display string immediately
    public void reciprocal()
    {
        try
        {
            result = new BigDecimal(displayString.toString());
            result = new BigDecimal(1.0f / (result.floatValue()));
            result = result.setScale(SCALE_TWO_DECIMAL, BigDecimal.ROUND_HALF_UP);
            displayString = new StringBuffer(result.toString());
        } catch ( Exception e )
        {
            displayString = new StringBuffer(ERROR_DIVISION_ZERO);
        }
        setChanged();
        notifyObservers();
    }

    // unary operator sign change, so operate on display string immediately
    public void signChange()
    {
        if ( !displayString.toString().equals(new String("0")) )
        {
            BigDecimal temp = new BigDecimal(displayString.toString());
            temp = temp.divide(new BigDecimal(-1.00f), BigDecimal.ROUND_HALF_UP);
            displayString = new StringBuffer(temp.toString());
            setChanged();
            notifyObservers();
        }
    }

    // add a decimal to the end of the displayed number
    public void decimalPoint()
    {
        // only append a decimal if a decimal does not already exist
        if ( displayString.toString().lastIndexOf('.') == -1 )
        {
            displayString.append(".");
            setChanged();
            notifyObservers();
        }
    }

    // perform a bitwise AND
    public void bitwiseAnd()
    {
        if ( currentOperator == Operator.EQUALS )
        {
            result = new BigDecimal(displayString.toString());
            displayString = new StringBuffer("0");
            setChanged();
            notifyObservers();
            currentOperator = Operator.AND;
        }
    }

    // perform a bitwise OR
    public void bitwiseOr()
    {
        if ( currentOperator == Operator.EQUALS )
        {
            result = new BigDecimal(displayString.toString());
            displayString = new StringBuffer("0");
            setChanged();
            notifyObservers();
            currentOperator = Operator.OR;
        }
    }

    // operate display string to result using current operator
    public void equals()
    {
        // operate display string to result based on current operator
        operate();

        // set current operator to EQUALS so next operator button press does not operate display string to result right away
        currentOperator = Operator.EQUALS;

    }

    // operate display string to result based on current operator
    public void operate()
    {
        try
        {
            switch ( currentOperator )
            {

                case DIVIDE:
                    result = result.divide(new BigDecimal(displayString.toString()), SCALE_TWO_DECIMAL, BigDecimal.ROUND_HALF_UP);
                    displayString = new StringBuffer(result.toString());
                    break;

                case TIMES:
                    result = result.multiply(new BigDecimal(displayString.toString()));
                    break;

                case MINUS:
                    result = result.subtract(new BigDecimal(displayString.toString()));
                    break;

                case PLUS:
                    result = result.add(new BigDecimal(displayString.toString()));
                    break;

                case AND:
                    // get BigInteger values of result and display string and perform bitwise AND
                    BigInteger andValue1 = result.toBigInteger();
                    BigInteger andValue2 = new BigInteger(displayString.toString());
                    result = new BigDecimal(andValue1.and(andValue2));
                    break;

                case OR:
                    // get BigInteger values of result and display string and perform bitwise OR
                    BigInteger orValue1 = result.toBigInteger();
                    BigInteger orValue2 = new BigInteger(displayString.toString());
                    result = new BigDecimal(orValue1.or(orValue2));
                    break;

                default:
                    break;
            }
            // trim trailing decimal places
            result = result.setScale(SCALE_TWO_DECIMAL, BigDecimal.ROUND_HALF_UP);

            // display strSystem.out.println(e.getMessage())ing and notify observers
            displayString = new StringBuffer(result.toString());
            setChanged();
            notifyObservers();
        }
        catch ( Exception e )
        {
            displayString = new StringBuffer(ERROR_DIVISION_ZERO);
            setChanged();
            notifyObservers();
            System.out.println(e.getMessage());
        }
    }




}   // end of class CalculatorModel extends Observable
