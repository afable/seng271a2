package seng271a2;

import java.awt.*;
import java.util.Observer;
import java.util.Observable;
import javax.swing.*;

public class OutputPanelDefault extends JPanel implements CalculatorColors, Observer
{
    JTextField output;
    
    final public static int TEXT_FIELD_WIDTH = 28;
    final public static int TEXT_FIELD_SIZE = 16;
    final public static String OVERFLOW_CHAR = "#";
    
    OutputPanelDefault()
    {
        output = new JTextField("0", TEXT_FIELD_WIDTH);
        output.setFont(new Font("Courier", Font.PLAIN, TEXT_FIELD_SIZE));
        output.setHorizontalAlignment(JTextField.RIGHT);
        
        // Place in the panel
        this.add(output);
        this.setBackground(backgroundColor);
    }

    /**
     * Description: update method notified when observers change in model-view paradigm
     *
     * An observable object can have one or more observers. An observer may be
     * any object that implements interface Observer. After an observable
     * instance changes, an application calling the Observable's notifyObservers
     * method causes all of its observers to be notified of the change by a
     * call to their update method.
     *
     * Basically, we generally don't have to change this since the output panel
     * is always observing the calculator model and will always display the
     * proper output when the calculator model notifies observers.
     */
    public void update(Observable model, Object obj)    // obj is just an argument passed by notifyObservers() method
    {
        String value = ((CalculatorModel)model).getValue();
        output.setText(value);
    }
}   // end of class OutputPanelDefault extends JPanel implements CalculatorColors, Observer
